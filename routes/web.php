<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home']);

/*
 * USUARIO(CONSULTORES)
 */
Route::get('/consultores', ['uses' => 'UsuarioController@consultorList', 'as' => 'consultores']);
Route::get('/consultores/checkbox/{usuario}', ['uses' => 'UsuarioController@checkbox', 'as' => 'consultor.checkbox']);
Route::post('/consultores/periodo', ['uses' => 'UsuarioController@setPeriodo', 'as' => 'consultor.periodo']);
Route::get('/consultores/relatorio', ['uses' => 'UsuarioController@relatorio', 'as' => 'consultor.relatorio']);
Route::get('/consultores/grafico', ['uses' => 'UsuarioController@graficoView', 'as' => 'consultor.grafico']);
Route::get('/consultores/graficojson', ['uses' => 'UsuarioController@graficoJson', 'as' => 'consultor.graficojson']);
Route::get('/consultores/pizza', ['uses' => 'UsuarioController@pizzaView', 'as' => 'consultor.pizza']);
Route::get('/consultores/pizzajson', ['uses' => 'UsuarioController@pizzaJson', 'as' => 'consultor.pizzajson']);

/*
 * CLIENTES
 */
Route::get('/clientes', ['uses' => 'ClienteController@index', 'as' => 'clientes']);
Route::get('/clientes/checkbox/{cliente}', ['uses' => 'ClienteController@checkbox', 'as' => 'cliente.checkbox']);
Route::post('/clientes/periodo', ['uses' => 'ClienteController@setPeriodo', 'as' => 'cliente.periodo']);
Route::get('/clientes/relatorio', ['uses' => 'ClienteController@relatorio', 'as' => 'cliente.relatorio']);
Route::get('/clientes/grafico', ['uses' => 'ClienteController@graficoView', 'as' => 'cliente.grafico']);
Route::get('/clientes/graficojson', ['uses' => 'ClienteController@graficoJson', 'as' => 'cliente.graficojson']);
Route::get('/clientes/pizza', ['uses' => 'ClienteController@pizzaView', 'as' => 'cliente.pizza']);
Route::get('/clientes/pizzajson', ['uses' => 'ClienteController@pizzaJson', 'as' => 'cliente.pizzajson']);
