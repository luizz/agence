define(['chart', 'ajax'], function (Chart) {
    var self, grafico;



    function _init() {
        $.getJSON('/clientes/graficojson', gerar);
    }

    function gerar(dados) {

        var _labels = [];
        var _value = [];
        var _color = [];
        var _datasets = [];

        $.each(dados.label, function (k, lab) {
            _labels.push(lab);
        });
        
        $.each(dados.value, function (k, v) {
            
            _value[k] = _value[k] || [];
            _color[k] = _color[k] || random_color();
            
            $.each(this, function (a, b) {
                _value[k].push(b);
                
            });
        });
        
          
        $.each(dados.client, function (ususuario, nome) {
     
            _datasets.push({
                label: nome,
                data: _value[ususuario],
                backgroundColor: _color[ususuario],
                borderColor: _color[ususuario],
                borderWidth: 1,
                fill:false
            });
        });

        var data = {
            labels: _labels,
            datasets: _datasets
        };

        var options = {
            maintainAspectRatio: false,
            spanGaps: false,
            elements: {
                line: {
                    tension: 0.000001
                }
            },
            scales: {
                yAxes: [{
                        stacked: true
                    }]
            },
            plugins: {
                filler: {
                    propagate: false
                },
                samples_filler_analyser: {
                    target: 'chart-analyser'
                }
            }
        };

        var chart = new Chart(grafico, {
            type: 'line',
            data: data,
            options: options
        });

    }

    return function () {

        self = $(this);
        grafico = document.getElementById("grafico").getContext('2d');
        _init();
    };
});