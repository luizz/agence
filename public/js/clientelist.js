define(['ajax'], function () {
    var self;

    function check() {

        var _tr = $(this);
        var input = _tr.find('input.usercheck');
        var user = input.data().check;

        _tr.toggleClass('active');

        input[0].checked = !input[0].checked;
        _tr.off();
        $.getJSON('/clientes/checkbox/' + user, function () {
            _tr.one('click', 'tr', check);
        });
    }

    function filtro() {
        var _fild = $(this);
        var form = _fild.closest('form');
        var data = form.serializeArray();
        $.post('/clientes/periodo', data, function (save) {
            if (save && save.aano) {
                form.find('[name="aano"]').val(save.aano);
            }
        }, 'json');
    }

    return function () {
        self = $(this);
        self.find('#filtro').off().on('change', 'input, select', filtro).find('select:first').trigger('change');
        self.off().on('click', 'tr', check);
    };
});