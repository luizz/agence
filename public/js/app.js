define('app', ['jquery'], function () {

    var jQuery = $ = require('jquery'),
            el = {
                load: jQuery("[data-controller]")
            },
    init = {};

    init.frame_breaker = function () {
        if (window.location.host !== window.top.location.host) {
            window.top.location.host = window.location.host;
        }
    };

    function construct() {

        el = {
            load: jQuery("[data-controller]")
        };

        for (var i in init) {

            try {
                init[i]();

            } catch (err) {

                log("Erro no app.init." + i + " " + "Descricao do erro: " + err.description + "Exception: " + err);
            }
        }

        if ((el.load instanceof jQuery) === true && el.load.length) {

            el.load.each(function () {

                var _contex = this;

                var reqSplit = jQuery(_contex).attr('data-controller').split('|');

                $.each(reqSplit, function (i, item) {
                    var req = item;
                    require([req], function (_func) {

                        try {
                            if (_func.call)
                                _func.call(_contex);
                            var r = _contex.removeAttribute('data-controller');
                        } catch (e) {

                            console.log(req + ' Try Error =>', e);
                            _contex.setAttribute('error', e);
                        } finally {

                            console.log(req + ' finally =>', r);
                            _contex.setAttribute('finally', r);
                        }

                    });
                });


            });
        }
    }

    return function () {

        construct();
    };

});