define(['chart', 'ajax'], function (Chart) {
    var self, grafico;
    
    
    
    function _init() {
        $.getJSON('/consultores/graficojson', gerar);
    }

    function gerar(dados) {
        var _labels = [];
        var _datasets = [];
        var _value = [];
        var _color = [];

        $.each(dados.label, function () {
            _labels.push(this);
        });

        $.each(dados.value, function (k, v) {
            
            _value[k] = _value[k] || [];
            _color[k] = _color[k] || random_color();
            
            $.each(this, function (a, b) {
                _value[k].push(b);
                
            });
        });

        $.each(dados.user, function (ususuario, nome) {
     

            _datasets.push({
                label: nome,
                data: _value[ususuario],
                backgroundColor: _color[ususuario],
                borderColor: _color[ususuario],
                borderWidth: 1
            });
        });

        var myChart = new Chart(grafico, {
            type: 'bar',
            data: {
                labels: _labels,
                datasets: _datasets
            },
            options: {
                responsive: true,
                scales: {
                    yAxes: [
                        {ticks: {beginAtZero: true}}
                    ]
                }
            }
        });

    }

    return function () {

        self = $(this);
        grafico = document.getElementById("grafico").getContext('2d');
        _init();
    };
});