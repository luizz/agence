(function(){

if (!window.console) {
    return;
}

var i = 0;
if (!i) {
    setTimeout(function () {
            console.log("%cBem Vindo!", "font: 3em sans-serif; color: green; ");
            console.log("%cOlá e bem vindo ao console. Se esta é a sua primeira vez aqui, sinta-se em casa. (Caso contrário, ignore esta mensagem!)", "font: 1.5em sans-serif; color: black;");
            console.log("%cO que você pode encontrar aqui é o que está debaixo da web: seu código. Você pode ler, escrever, reescrever e manipular este código para tornar a web sua.", "font: 1.5em sans-serif; color: black;");
            console.log("%cVocê pode descobrir mais sobre como usar o console a partir desses sites:", "font: 1.25 sans-serif; color: black;");
            console.log("%c - Firefox: https://developer.mozilla.org/en-US/docs/Tools/Web_Console", "font: 1.25 sans-serif; color: black;");
            console.log("%c - Chrome: https://developers.google.com/web/tools/chrome-devtools/console/", "font: 1.25 sans-serif; color: black;");
            console.log("%c - Safari: https://developer.apple.com/library/content/documentation/AppleApplications/Conceptual/Safari_Developer_Guide/Console/Console.html", "font: 1.25 sans-serif; color: black;");
            console.log("%c - Ou apenas use qualquer mecanismo de pesquisa para procurar 'developer console'", "font: 1.25 sans-serif; color: black;");
            console.log("%cPara obter ajuda com o aprendizado de código, tente https://www.codecademy.com/pt-BR/", "font: 1.25 sans-serif; color: black;");
            console.log("%c[Este texto gerado por https://github.com/stml/welcomejs]", "font: 1 sans-serif; color: grey;");
        }, 1);
    i = 1;
}

})();
