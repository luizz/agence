define(['chart', 'ajax'], function (Chart) {

    var self, pizza;

    function _init() {

        $.getJSON('/clientes/pizzajson', gerar);

    }

    function gerar(dados) {
        
        var _label = [];
        var _data = [];
        var _color = [];
        
        $.each(dados.value, function(us, val){
            _data.push(val);
        })
        
        $.each(dados.label, function(us, val){
            _label.push(val);
            _color.push(random_color());
        })
        
        var config = {
            type: 'pie',
            data: {
                datasets: [{
                        data: _data,
                        backgroundColor: _color,
                        label: 'Pizza'
                    }],
                labels: _label
            },
            options: {
                responsive: true
            }
        };

        var myChart = new Chart(pizza, config);

    }

    return function () {

        self = $(this);
        pizza = document.getElementById("pizza").getContext('2d');
        _init();
    };
});