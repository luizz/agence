@extends('layout')

@section('title', 'Gráfico')

@section('conteudo')

<table class="table"> 
    <caption><a href="{{ redirect()->back()->getTargetUrl() }}">Clientes</a> >> Gráfico </caption>
</table>

<div data-controller="clientegrafico">
    <canvas  id="grafico" width="400" height="200"></canvas>
</div>


@endsection