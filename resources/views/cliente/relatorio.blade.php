@extends('layout')

@section('title', 'Relatório')

@section('conteudo')

<table class="table table-hover table-bordered table-responsive"> 
    <caption><a href="{{ redirect()->back()->getTargetUrl() }}">Consultores</a> >> Relatório</caption>
    <thead> 
            <tr class="active"> 
                <th>Período</th>
                @foreach($clientes as $cliente)
                <th data-id="{{$cliente->co_cliente}}">{{ str_limit($cliente->no_razao, 15) }}</th>
                @endforeach
                <th>TOTAL</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $somatotal = 0;
            //$somaclientes = array();
            
            ?>
            @foreach($periodos as $periodo)
            <tr>
                <?php
                
                $somacliente = (float)0;
                ?>
                <td  class="text-left">{{$periodo['name']}}</td>
                @foreach($clientes as $cliente)
                <?php
                    $receita = $cliente->receita($periodo['date']);
                    $somacliente = bcadd($cliente->receita , $somacliente, 2);

                    //$somaclientes[$cliente->co_cliente] += floatval($receita);
                ?>
                <td class="text-right">{{$receita}}</td>
                @endforeach
                <td class="text-right">{{ $formatter->formatCurrency($somacliente, 'BRL') }}</td>
                <?php $somatotal += floatval($somacliente) ?>
            </tr>
            @endforeach
            <tr class="active"> 
                <td>TOTAL</td>
                @foreach($clientes as $cliente)
                <td class="text-right"></td>
                @endforeach
                <td class="text-right">{{ $formatter->formatCurrency($somatotal, 'BRL') }}</td>
            </tr>
        </tbody> 
</table>

@endsection