@extends('layout')

@section('title', 'Clientes')

@section('conteudo')

<div data-controller="clientelist">    
    @include('cliente.filtro') 

    <table class="table table-hover table-bordered table-responsive"> 
        <caption>Lista de Clientes</caption>
        <thead> 
            <tr> 
                <th>
                    #
                </th>
                <th>Razão Social</th>
            </tr>
        </thead>
        <tbody>
            @forelse($clientes as $cliente)
            <tr {{ isset($clientechecks[$cliente->co_cliente]) ? 'class=active':null }}>
                <td>
                    <input type="checkbox" class="usercheck" data-check="{{ $cliente->co_cliente }}" {{ isset($clientechecks[$cliente->co_cliente]) ? 'checked=true':null }}/>
                </td>
                <td>{{ $cliente->no_razao  }}</td>
            </tr> 
            @empty
            <tr><td colspan="2">Nenhum cliente</td></tr>
            @endforelse
        </tbody> 
    </table>
    <div class="text-center">
        {{ $clientes->links() }}
    </div>
</div>
@endsection