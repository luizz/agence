@extends('layout')

@section('title', 'Pizza')

@section('conteudo')

<table class="table"> 
    <caption><a href="{{ redirect()->back()->getTargetUrl() }}">Clientes</a> >> Pizza </caption>
</table>

<div data-controller="clientepizza">
    <canvas id="pizza" width="400" height="200"></canvas>
</div>

@endsection