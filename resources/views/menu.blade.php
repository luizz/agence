<nav class="navbar navbar-default">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}" title="CAOL">
            <img src="/img/agence.png" style="max-height: 90%">
        </a>
        
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
         <span class="sr-only">Toggle</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
        </button>
        
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav" data-current="{{$current}}">
                <li class="nav-item {{ strpos($current, 'home') !== false ? 'active':'' }}">
                    <a class="nav-link" href="{{ route('home') }}">Home</a>
                </li>
                <li class="nav-item {{ strpos($current, 'consultor') !== false ? 'active':'' }}">
                    <a class="nav-link" href="{{ route('consultores') }}">Consultores</a>
                </li>
                <li class="nav-item {{ strpos($current, 'cliente') !== false ? 'active':'' }}">
                    <a class="nav-link" href="{{ route('clientes') }}">Clientes</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
