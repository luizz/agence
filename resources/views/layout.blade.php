<!doctype html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="HandheldFriendly" content="true">
        <meta name="googlebot" content="noindex">
        <meta name="robots" content="noindex,nofollow">
        <link type="text/plain" rel="author" href="/humans.txt" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="msapplication-tap-highlight" content="no" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="#00A083">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon" /> 
        <title>@yield('title') - Controle de Atividades Online - Agence Consultoria Web</title>
        <meta name="description" content="Controle de Atividades Online - Agence Consultoria Web">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}?{{ rand().date('YmdHi') }}">
        <!--[if lt IE 9]>
            <script src="/js/html5.js" async></script>
            <script src="/js/respond.js" async></script>
        <![endif]-->
        <script type="text/javascript" src="https://cdn.polyfill.io/v2/polyfill.js" async></script>
        <script src="{{ asset('js/browser_selector.js') }}" defer></script>
        <script src="{{ asset('js/ie-behavior.js') }}" defer></script>
        <script src="{{ asset('js/require.js') }}" data-main="{{ asset('js/app') }}"></script>
        <script defer>
            requirejs(["app"], function (app) {app();});
        </script>
    </head>
    <body>
        <header>
            @include('menu') 
        </header>
        <section class="container">
            <div class="row">
                @yield('conteudo')  
            </div>
        </section>
        <footer></footer>

    </body>
</html>
