@extends('layout')

@section('title', 'Gráfico')

@section('conteudo')

<table class="table"> 
    <caption><a href="{{ redirect()->back()->getTargetUrl() }}">Consultores</a> >> Gráficos </caption>
</table>

<div data-controller="usuariografico">
    <canvas  id="grafico" width="400" height="200"></canvas>
</div>

@endsection