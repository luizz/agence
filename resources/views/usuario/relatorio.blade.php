@extends('layout')

@section('title', 'Relatório')

@section('conteudo')

<div class="table"> 
    <caption>
        <a href="{{ redirect()->back()->getTargetUrl() }}">Consultores</a> >> Relatório
    </caption>
</div>

@foreach($usuarios as $usuario)
<table class="table table-hover table-bordered">
    <thead>
        <tr class="active"> 
            <th colspan="5">{{ $usuario->no_usuario.' / '.$usuario->slug }}</th>
        </tr>
        <tr> 
            <th class="text-center">Período</th>
            <th class="text-center">Receita Líquida</th>
            <th class="text-center">Custo Fixo</th>
            <th class="text-center">Comissão</th>
            <th class="text-center">Lucro</th>
        </tr>
    </thead>
    <tbody>
        
        <?php
        
        $somareceita = 0;
        $somacustofixo = 0;
        $somacomissao = 0;
        $somalucro = 0;
        $formatter = new \NumberFormatter('pt_BR', \NumberFormatter::CURRENCY);
        ?>
        
        @foreach($usuario->periodo() as $periodo)
        <?php
            $lucro = 0;
         
            
            $receita = $usuario->receita($periodo['date']);
            $comissao = $usuario->comissao($periodo['date']);
            $custofixo = $usuario->custofixo();
            
            //Calculos
 
            $somareceita += floatval($usuario->receita);
            $somacustofixo += floatval($usuario->custofixo);
            $somacomissao += floatval($usuario->comissao);
            $lucro = $usuario->lucro();
            $somalucro = bcadd($somalucro,$usuario->lucro,2);

        ?>
        <tr> 
            <td>{{$periodo['name']}}</td>
            <td class="text-right">
                {{ $receita['valor'] }}
            </td>
            <td class="text-right">{{ $custofixo }}</td>
            <td class="text-right">
                {{ $comissao['valor']}}
            </td>
            <td class="text-right">{{ $lucro }}</td>
        </tr>
        @endforeach
        <tr class="active"> 
            <td>SALDO</td>
            <td class="text-right">{{ $formatter->formatCurrency($somareceita, 'BRL') }}</td>
            <td class="text-right">{{ $formatter->formatCurrency($somacustofixo, 'BRL') }}</td>
            <td class="text-right">{{ $formatter->formatCurrency($somacomissao, 'BRL') }}</td>
            <td class="text-right">{{ $formatter->formatCurrency($somalucro, 'BRL') }}</td>
        </tr>
    </tbody> 
</table>
@endforeach

@endsection