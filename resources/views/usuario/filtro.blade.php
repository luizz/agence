<?php

$inmes = isset($usuarioperiodo['inmes']) ? $usuarioperiodo['inmes']:NULL;
$inano = isset($usuarioperiodo['inano']) ? $usuarioperiodo['inano']:NULL;
$ames = isset($usuarioperiodo['ames']) ? $usuarioperiodo['ames']:NULL;
$aano = isset($usuarioperiodo['aano']) ? $usuarioperiodo['aano']:NULL;
$yearbase = 2006;
$year = date('Y');

?>
<div class="filtro">
    <div class="col-md-7">
        <form id="filtro" class="form-inline">
            <div class="form-group">
                <label class="col-sm-12 control-label">Período</label>
            </div>
            <div class="form-group">
                <select class="form-control" name="inmes">
                    <option value="1"{{ $inmes == 1 ? ' selected':'' }}>Jan</option>
                    <option value="2"{{ $inmes == 2 ? ' selected':'' }}>Fev</option>
                    <option value="3"{{ $inmes == 3 ? ' selected':'' }}>Mar</option>
                    <option value="4"{{ $inmes == 4 ? ' selected':'' }}>Abr</option>
                    <option value="5"{{ $inmes == 5 ? ' selected':'' }}>Mai</option>
                    <option value="6"{{ $inmes == 6 ? ' selected':'' }}>Jun</option>
                    <option value="7"{{ $inmes == 7 ? ' selected':'' }}>Jul</option>
                    <option value="8"{{ $inmes == 8 ? ' selected':'' }}>Ago</option>
                    <option value="9"{{ $inmes == 9 ? ' selected':'' }}>Set</option>
                    <option value="10"{{ $inmes == 10 ? ' selected':'' }}>Out</option>
                    <option value="11"{{ $inmes == 11 ? ' selected':'' }}>Nov</option>
                    <option value="12"{{ $inmes == 12 ? ' selected':'' }}>Dez</option>
                </select>
                <select class="form-control" name="inano">
                    @for ($i = $year; $i > $yearbase; $i--)
                        <option value="{{ $i }}"{{ $inano == $i ? ' selected':'' }}>{{ $i }}</option>
                    @endfor
                </select>
            </div>
            <div class="form-group">
                <label class="col-sm-12 control-label"> à </label>
            </div>
            <div class="form-group">
                <select class="form-control" name="ames">
                    <option value="1"{{ $ames == 1 ? ' selected':'' }}>Jan</option>
                    <option value="2"{{ $ames == 2 ? ' selected':'' }}>Fev</option>
                    <option value="3"{{ $ames == 3 ? ' selected':'' }}>Mar</option>
                    <option value="4"{{ $ames == 4 ? ' selected':'' }}>Abr</option>
                    <option value="5"{{ $ames == 5 ? ' selected':'' }}>Mai</option>
                    <option value="6"{{ $ames == 6 ? ' selected':'' }}>Jun</option>
                    <option value="7"{{ $ames == 7 ? ' selected':'' }}>Jul</option>
                    <option value="8"{{ $ames == 8 ? ' selected':'' }}>Ago</option>
                    <option value="9"{{ $ames == 9 ? ' selected':'' }}>Set</option>
                    <option value="10"{{ $ames == 10 ? ' selected':'' }}>Out</option>
                    <option value="11"{{ $ames == 11 ? ' selected':'' }}>Nov</option>
                    <option value="12"{{ $ames == 12 ? ' selected':'' }}>Dez</option>
                </select>
                <select class="form-control" name="aano">
                    @for ($ii = $year; $ii > $yearbase; $ii--)
                        <option value="{{ $ii }}"{{ $aano == $ii ? ' selected':'' }}>{{ $ii }}</option>
                    @endfor
                </select>
            </div>
        </form>

    </div>
    <div class="col-md-3">
        <a href="{{ route('consultor.relatorio') }}" class="btn btn-primary">Relatório</a>
        <a href="{{ route('consultor.grafico') }}" class="btn btn-success">Gráfico</a>
        <a href="{{ route('consultor.pizza') }}" class="btn btn-info">Pizza</a> 
    </div>
</div>