@extends('layout')

@section('title', 'Consultores')

@section('conteudo')

<div data-controller="usuariolist">    
    @include('usuario.filtro') 

    <table class="table table-hover table-bordered">
        <caption>Lista de Consultores</caption>
        <thead> 
            <tr> 
                <th>
                    #
                </th>
                <th>Selecione os consultores</th>
            </tr>
        </thead>
        <tbody>
            @forelse($usuarios as $usuario)
            <tr {{ isset($usuariochecks[$usuario->slug]) ? 'class=active':null }}>
                <td>
                    <input type="checkbox" class="usercheck" data-check="{{ $usuario->slug }}" {{ isset($usuariochecks[$usuario->slug]) ? 'checked=true':null }}/>
                </td>
                <td>{{ $usuario->no_usuario }}</td> 
            </tr> 
            @empty
            <tr><td colspan="2">Nenhum consultor</td></tr>
            @endforelse
        </tbody> 
    </table>
    <div class="text-center">
        {{ $usuarios->links() }}
    </div>
</div>

@endsection