<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Usuario extends Model {

    public $timestamps = false;
    protected $table = 'cao_usuario';
    protected $primaryKey = 'co_usuario';
    protected $with = [];
    protected $fillable = [
        'co_usuario',
        'no_usuario'
    ];

    public function getSlugAttribute() {
        return $this->attributes['co_usuario'];
    }
  
    public function scopeAtivo($builder) {
        return $builder->whereRaw("co_usuario IN ( SELECT p.co_usuario FROM permissao_sistema p WHERE p.co_sistema = 1 AND p.in_ativo = 'S' AND p.co_tipo_usuario IN (0,1,2) )");
    }

    /**
     * Somente Usuario que foram checkados
     *
     * @param $builder
     */
    public function scopeCheck($builder) {

        $usuariochecks = (array) unserialize(session('usuariochecks'));

        if (count($usuariochecks) > 0) {
            $usernames = array_keys($usuariochecks);
            $builder->whereRaw("co_usuario IN ('" . implode("','", $usernames) . "')");
        } else {
            $builder->whereRaw("co_usuario = 0");
        }

        return $builder;
    }

    /**
     * Seta os periodo
     */
    public function periodo() {

        $usuarioperiodo = (array) unserialize(session('usuarioperiodo'));
        $periodos = [];

        if (count($usuarioperiodo) > 0) {

            $inmes = isset($usuarioperiodo['inmes']) ? $usuarioperiodo['inmes'] : NULL;
            $inano = isset($usuarioperiodo['inano']) ? $usuarioperiodo['inano'] : NULL;

            $ames = isset($usuarioperiodo['ames']) ? $usuarioperiodo['ames'] : NULL;
            $aano = isset($usuarioperiodo['aano']) ? $usuarioperiodo['aano'] : NULL;

            $dataInicio = new \DateTime($inano . '-' . sprintf($inmes, '%02f') . '-01');
            $dataFim = new \DateTime($aano . '-' . sprintf($ames, '%02f') . '-01');

            $periodos[] = ['name' => $dataInicio->format('M Y'), 'date' => $dataInicio->format('Y-m-d')];

            while ($dataInicio != $dataFim) {
                $dataInicio->add(new \DateInterval('P1M'));
                $periodos[] = ['name' => $dataInicio->format('M Y'), 'date' => $dataInicio->format('Y-m-d')];
            }
        }

        return $periodos;
    }

    /**
     * Seta os receita
     */
    public function receita($periodo) {

        $data = new \DateTime($periodo);
        $co_usuario = $this->attributes['co_usuario'];

        $faturas = DB::table('cao_fatura')
                ->whereRaw("DATE_FORMAT(cao_fatura.data_emissao, \"%Y-%m\") = '" . $data->format('Y-m') . "'")
                ->join('cao_os', function ($join) use ($co_usuario) {
            $join->on('cao_os.co_os', '=', 'cao_fatura.co_os')
            ->where('cao_os.co_usuario', $co_usuario);
        });

        $sql = ''; //$faturas->toSql();
        $faturas = $faturas->get();

        $valor = 0;
        $totalimp = 0;
        $total = $faturas->count();

        foreach ($faturas as $fatura) {

            $impinc = 0;

            if ($fatura->total_imp_inc > 0)
                $impinc = (($fatura->valor * $fatura->total_imp_inc) / 100);

            $totalimp = $totalimp + $impinc;
            $valor = ($valor + ($fatura->valor - $impinc));
        }
        $formatter = new \NumberFormatter('pt_BR', \NumberFormatter::CURRENCY);
        $this->attributes['receita'] = $valor;
        return ['total' => $total, 'notvalor' => $valor, 'valor' => $formatter->formatCurrency($valor, 'BRL'), 'totalimp' => $totalimp, 'sql' => $sql];
    }

    /**
     * Seta os custo
     */
    public function custofixo() {

        $co_usuario = $this->attributes['co_usuario'];

        $brut_salario = DB::table('cao_salario')->where('co_usuario', $co_usuario)->value('brut_salario');
        $formatter = new \NumberFormatter('pt_BR', \NumberFormatter::CURRENCY);
        $this->attributes['custofixo'] = $brut_salario;
        return $formatter->formatCurrency($brut_salario, 'BRL');
    }

    /**
     * Seta os comissao
     */
    public function comissao($periodo) {

        $data = new \DateTime($periodo);
        $co_usuario = $this->attributes['co_usuario'];

        $faturas = DB::table('cao_fatura')
                ->whereRaw("DATE_FORMAT(cao_fatura.data_emissao, \"%Y-%m\") = '" . $data->format('Y-m') . "'")
                ->join('cao_os', function ($join) use ($co_usuario) {
            $join->on('cao_os.co_os', '=', 'cao_fatura.co_os')
            ->where('cao_os.co_usuario', $co_usuario);
        });

        $faturas = $faturas->get();

        $valor = 0;
        $total = $faturas->count();
        $comissao = 0;
        foreach ($faturas as $fatura) {

            $impinc = 0;

            if ($fatura->total_imp_inc > 0)
                $impinc = (($fatura->valor * $fatura->total_imp_inc) / 100);


            $comissao += $fatura->comissao_cn;

            $valorsemimp = ($fatura->valor - $impinc);
            $valor += (($valorsemimp * $fatura->comissao_cn) / 100);
            //Multiplicar o resultado pelo valor é algo estranho! : $valorsemimp * 
        }
        
        $formatter = new \NumberFormatter('pt_BR', \NumberFormatter::CURRENCY);
        $this->attributes['comissao'] = $valor;
        return ['total' => $total, 'valor' => $formatter->formatCurrency($valor, 'BRL'), 'comissao' => $comissao];
    }

    /**
     * Seta os lucro
     */
    public function lucro() {
        $formatter = new \NumberFormatter('pt_BR', \NumberFormatter::CURRENCY);
        $this->attributes['lucro'] = ($this->attributes['receita'] - ( $this->attributes['comissao'] + $this->attributes['custofixo']));
        return $formatter->formatCurrency($this->attributes['lucro'], 'BRL');
    }

}
