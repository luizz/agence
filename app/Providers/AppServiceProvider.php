<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        
        view()->composer('menu', function ($view) {

            $current = \Request::route()->getName();

            $view->with(compact('current'));
        });
        
        view()->composer('usuario.filtro', function ($view) {

            $usuarioperiodo = (array) unserialize(session('usuarioperiodo'));

            $view->with(compact('usuarioperiodo'));
        });
        
        view()->composer('cliente.filtro', function ($view) {

            $clienteperiodo = (array) unserialize(session('clienteperiodo'));

            $view->with(compact('clienteperiodo'));
        });
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
