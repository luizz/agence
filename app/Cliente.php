<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cliente extends Model {

    public $timestamps = false;
    protected $table = 'cao_cliente';
    protected $primaryKey = 'co_cliente';
    protected $with = [];
    protected $fillable = [
        'no_razao',
        'no_fantasia',
        'no_contato',
        'nu_telefone'
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName() {
        return $this->co_cliente;
    }

    public function scopeTipo($builder, $tipo = 'A') {
        return $builder->where('tp_cliente', $tipo);
    }

    /**
     * Seta os periodo
     */
    public function scopePeriodo() {

        $clienteperiodo = (array) unserialize(session('clienteperiodo'));
        $periodos = [];

        if (count($clienteperiodo) > 0) {

            $inmes = isset($clienteperiodo['inmes']) ? $clienteperiodo['inmes'] : 1;
            $inano = isset($clienteperiodo['inano']) ? $clienteperiodo['inano'] : 2006;

            $ames = isset($clienteperiodo['ames']) ? $clienteperiodo['ames'] : 1;
            $aano = isset($clienteperiodo['aano']) ? $clienteperiodo['aano'] : 2006;

            $dataInicio = new \DateTime($inano . '-' . sprintf($inmes, '%02f') . '-01');
            $dataFim = new \DateTime($aano . '-' . sprintf($ames, '%02f') . '-01');

            $periodos[] = ['name' => $dataInicio->format('M Y'), 'date' => $dataInicio->format('Y-m-d')];

            while ($dataInicio != $dataFim) {
                $dataInicio->add(new \DateInterval('P1M'));
                $periodos[] = ['name' => $dataInicio->format('M Y'), 'date' => $dataInicio->format('Y-m-d')];
            }
        }

        return $periodos;
    }

    /**
     * Obtem á receita
     *
     * @param float
     */
    public function receita($periodo) {

        $data = new \DateTime($periodo);
        $co_cliente = $this->attributes['co_cliente'];

        $faturas = DB::table('cao_fatura')
                        ->whereRaw("DATE_FORMAT(cao_fatura.data_emissao, \"%Y-%m\") = '" . $data->format('Y-m') . "'")
                        ->where('cao_fatura.co_cliente', $co_cliente)
                        ->join('cao_os', function ($join) {
                            $join->on('cao_os.co_os', '=', 'cao_fatura.co_os');
                        })->get();
        $valor = 0;
        $totalimp = 0;

        foreach ($faturas as $fatura) {
            $imp_inc = (($fatura->valor * $fatura->total_imp_inc) / 100);
            $totalimp = bcadd($totalimp, $imp_inc, 2);
            $valor = bcadd($valor, ($fatura->valor - $imp_inc), 2);
        }
        
        $this->attributes['receita'] = $valor;
        $formatter = new \NumberFormatter('pt_BR', \NumberFormatter::CURRENCY);
        return $formatter->formatCurrency($valor, 'BRL');
    }
    

    /**
     * Somente Cliente que foram checkados
     *
     * @param $builder
     */
    public function scopeCheck($builder) {

        $clientechecks = (array) unserialize(session('clientechecks'));

        if (count($clientechecks) > 0) {
            $clientekey = array_keys($clientechecks);
            return $builder->whereIn('co_cliente', $clientekey);
        }

        return $builder;
    }

}
