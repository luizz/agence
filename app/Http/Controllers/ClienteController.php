<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller {

    public function __construct() {
        
    }

    /**
     * Exibe a página listagem de clientes.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $clientes = Cliente::Tipo('A')->orderBy('no_razao', 'desc')->paginate(15);

        $clientechecks = (array) unserialize(session('clientechecks'));

        return view('cliente.list', compact('clientes', 'clientechecks'));
    }

    /**
     * Exibe a página relatorio.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function relatorio() {
        
        $periodos = Cliente::Periodo();
        
        $clientes = Cliente::Tipo('A')->check()->orderBy('no_razao', 'desc')->get();
        $formatter = new \NumberFormatter('pt_BR', \NumberFormatter::CURRENCY);
        
        return view('cliente.relatorio', compact('clientes', 'periodos', 'formatter'));
    }

    /**
     * Exibe a view em grafico.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function graficoView() {

        return view('cliente.grafico');
    }
    
    /**
     * Exibe a view em grafico.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function graficoJson() {
        
        $grafico = ['label' => [], 'client' => [], 'value' => []];
        
        $clienteFake = new Cliente();
        $clientes = Cliente::Tipo('A')->check()->orderBy('no_razao', 'desc')->get();
        
        foreach ($clienteFake->periodo() as $periodo) {

            foreach ($clientes as $cliente) {

                $receita = $cliente->receita($periodo['date']);

                $grafico['label'][$periodo['date']] = $periodo['name'];

                if (isset($grafico['client'][$cliente->co_cliente]) == false) {
                    $grafico['client'][$cliente->co_cliente] = $cliente->no_razao;
                }

                if (isset($grafico['value'][$cliente->co_cliente]) == false) {
                    $grafico['value'][$cliente->co_cliente] = [];
                }

                $grafico['value'][$cliente->co_cliente][$periodo['date']] = number_format($cliente->receita, 0, '', '');
            }
        }
        
        return response()->json($grafico);
    }

    /**
     * Exibe a view em pizza.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pizzaView() {
    
        return view('cliente.pizza');
    }
    
    /**
     * Exibe o resultado json para pizza.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pizzaJson() {
        
        $clientes = Cliente::Tipo('A')->check()->orderBy('no_razao', 'desc')->get();
        $clienteFake = new Cliente();
        $pizza = [];
        
        $receitatotal = 0;
        
        foreach ($clienteFake->periodo() as $periodo) {
            
            foreach ($clientes as $cliente) {

                $receita = $cliente->receita($periodo['date']);
                
                if(isset($pizza['label'][$cliente->co_cliente]) == false){
                    $pizza['label'][$cliente->co_cliente] = $cliente->no_razao;
                    $pizza['value'][$cliente->co_cliente] = 0;
                }
                
                $pizza['value'][$cliente->co_cliente] = bcadd($pizza['value'][$cliente->co_cliente], $cliente->receita, 2);
                
                $receitatotal = bcadd($receitatotal, $cliente->receita, 2);
                
            }
        }
        
        foreach ($pizza['label'] as $co_cliente => $no_razao) {
            $pizza['label'][$co_cliente] = $no_razao . ' ' . number_format((100 * $pizza['value'][$co_cliente]) / $receitatotal) . '%';
        }
        
        $pizza['total'] = $receitatotal;
        
        return response()->json($pizza);
    }

    /**
     * Salva cliente checkbox da página listagem de clientes.
     *
     * @return string
     */
    public function checkbox(Cliente $cliente) {

        $clientechecks = (array) unserialize(session('clientechecks'));

        if (isset($clientechecks[$cliente->co_cliente]) === false) {
            $clientechecks[$cliente->co_cliente] = true;
        } else {
            unset($clientechecks[$cliente->co_cliente]);
        }

        session(['clientechecks' => serialize($clientechecks)]);

        return response()->json($clientechecks);
    }

    /**
     * Salvar filtro de periodo da listagem de cliente.
     *
     * @return array
     */
    public function setPeriodo(Request $request) {

        $clienteperiodo = (array) unserialize(session('clienteperiodo'));

        $clienteperiodo['inmes'] = $request->input('inmes');
        $clienteperiodo['inano'] = $request->input('inano');
        $clienteperiodo['ames'] = $request->input('ames');
        $clienteperiodo['aano'] = $request->input('aano') < $clienteperiodo['inano'] ? $clienteperiodo['inano'] : $request->input('aano');

        session(['clienteperiodo' => serialize($clienteperiodo)]);

        return response()->json($clienteperiodo);
    }

}
