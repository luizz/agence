<?php

namespace App\Http\Controllers;

class HomeController extends Controller {

    public function __construct() {}

    /**
     * Exibe a página inicial.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('home');
    }


}
