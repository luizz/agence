<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;

class UsuarioController extends Controller {

    public function __construct() {
        
    }

    /**
     * Exibe a página listagem de consultores.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function consultorList() {

        $usuarios = Usuario::ativo()->paginate(15);

        $usuariochecks = (array) unserialize(session('usuariochecks'));

        return view('usuario.list', compact('usuarios', 'usuariochecks'));
    }

    /**
     * Exibe a página relatorio.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function relatorio() {

        $usuarios = Usuario::ativo()->check()->get();
        $formatter = new \NumberFormatter('pt_BR', \NumberFormatter::CURRENCY);
        
        return view('usuario.relatorio', compact('usuarios','formatter'));
    }

    /**
     * Exibe a view do grafico.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function graficoView() {

        return view('usuario.grafico');
    }

    /**
     * Exibe o resultado do grafico em json.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function graficoJson() {

        $usuarios = Usuario::ativo()->check()->get();
        $usuarioFake = new Usuario();
        $grafico = ['label' => [], 'user' => [], 'value' => []];

        foreach ($usuarioFake->periodo() as $periodo) {

            foreach ($usuarios as $usuario) {

                $receita = $usuario->receita($periodo['date']);

                $grafico['label'][$periodo['date']] = $periodo['name'];

                if (isset($grafico['user'][$usuario->slug]) == false) {
                    $grafico['user'][$usuario->slug] = $usuario->no_usuario;
                }

                if (isset($grafico['value'][$usuario->slug]) == false) {
                    $grafico['value'][$usuario->slug] = [];
                }

                $grafico['value'][$usuario->slug][$periodo['date']] = number_format($receita['notvalor'], 0, '', '');
            }
        }

        return response()->json($grafico);
    }

    /**
     * Exibe a view de pizza.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pizzaView() {
        
        return view('usuario.pizza');
    }

    /**
     * Exibe o resultado em pizza.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pizzaJson() {
        
        $usuarios = Usuario::ativo()->check()->get();
        $usuarioFake = new Usuario();
        $pizza = [];
        
        $receitatotal = 0;
        
        foreach ($usuarioFake->periodo() as $periodo) {
            
            foreach ($usuarios as $usuario) {

                $receita = $usuario->receita($periodo['date']);
                
                if(isset($pizza['label'][$usuario->slug]) == false){
                    $pizza['label'][$usuario->slug] = $usuario->no_usuario;
                    $pizza['value'][$usuario->slug] = 0;
                }
                
                $pizza['value'][$usuario->slug] = bcadd($pizza['value'][$usuario->slug], $receita['notvalor'], 2);
                $receitatotal = bcadd($receitatotal, $usuario->receita, 2);
            }
        }
        
        foreach ($pizza['label'] as $slug => $no_usuario) {
            $pizza['label'][$slug] = $no_usuario . ' ' . number_format((100 * $pizza['value'][$slug]) / $receitatotal) . '%';
        }
        
        return response()->json($pizza);
    }

    /**
     * Salvar usuarios checkados da página listagem de consultores.
     *
     * @return array
     */
    public function checkbox(Usuario $usuario) {

        $usuariochecks = (array) unserialize(session('usuariochecks'));

        if (isset($usuariochecks[$usuario->slug]) === false) {
            $usuariochecks[$usuario->slug] = true;
        } else {
            unset($usuariochecks[$usuario->slug]);
        }

        session(['usuariochecks' => serialize($usuariochecks)]);

        return response()->json($usuariochecks);
    }

    /**
     * Salvar filtro de periodo da listagem de consultores.
     *
     * @return array
     */
    public function setPeriodo(Request $request) {

        $usuarioperiodo = (array) unserialize(session('usuarioperiodo'));

        $usuarioperiodo['inmes'] = $request->input('inmes');
        $usuarioperiodo['inano'] = $request->input('inano');
        $usuarioperiodo['ames'] = $request->input('ames');
        $usuarioperiodo['aano'] = $request->input('aano') < $usuarioperiodo['inano'] ? $usuarioperiodo['inano'] : $request->input('aano');

        session(['usuarioperiodo' => serialize($usuarioperiodo)]);

        return response()->json($usuarioperiodo);
    }

}
